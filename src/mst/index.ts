import { types, Instance, SnapshotIn, flow, applySnapshot } from 'mobx-state-tree'
import * as uuid from 'uuid'
import api from 'axios'

const EmployeeModel = types.model('Employee', {
    id: types.optional(types.identifier, () => uuid.v4()),
    firstName: types.string,
    lastName: types.string,
    isHealthy: types.optional(types.boolean, true)
}).actions(self => {
    const toggleHealth = () => {
        self.isHealthy = !self.isHealthy
    }

    return { toggleHealth }
})

export const CompanyModel = types.model('Company', {
    id: types.optional(types.identifier, () => uuid.v4()),
    name: types.string,
    fromBE: types.maybe(types.boolean),
    employees: types.array(EmployeeModel)
}).preProcessSnapshot(snapShot => {
    if (!snapShot.fromBE) {
        return snapShot
    }

    const processEmployee = (employee: any) => ({
        firstName: employee.firstName || employee.first_name,
        lastName: employee.lastName || employee.last_name,
        isHealthy: employee.isHealthy || employee.is_healty
    })

    return {
        ...snapShot,
        fromBE: false,
        employees: snapShot.employees?.map(processEmployee)
    }
})
    .actions(self => {
        const addNewEmployee = (employee: EmployeeSnapshot) => {
            self.employees.push(employee)
        }

        const getEmployeesFromBE = flow(function* getEmployeesFromBE() {
            const response = yield api.get('http://localhost:3001/users')
            applySnapshot(self, {
                ...self,
                fromBE: true,
                employees: [...response.data]
            })
        })

        return { addNewEmployee, getEmployeesFromBE }
    }).views(self => ({
        get employeesTotal() {
            return self.employees.length
        },
        filterByName(query: string) {
            return self.employees.filter(employee => employee.firstName.includes(query) || employee.lastName.includes(query))
        }
    }))

const RootModel = types.model('Root', {
    company: CompanyModel
})

export { RootModel }

export type Root = Instance<typeof RootModel>
export type Company = Instance<typeof CompanyModel>
export type Employee = Instance<typeof EmployeeModel>

export type EmployeeSnapshot = SnapshotIn<typeof EmployeeModel>
