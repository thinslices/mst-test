import { RootModel } from "."
import { onSnapshot } from "mobx-state-tree"

const ROOT_STORAGE = 'rootStorage'

export const setupRootStore = () => {
    const defaultState = {
        company: {
            id: "1",
            name: 'Thinslices',
            employees: []
        }
    }
    const storageData = localStorage.getItem(ROOT_STORAGE)
    const stateFromStorage = storageData ? JSON.parse(storageData) : null

    const rootTree = RootModel.create(stateFromStorage || defaultState)

    onSnapshot(rootTree, snapshot => localStorage.setItem(ROOT_STORAGE, JSON.stringify(snapshot)))

    return { rootTree }
}