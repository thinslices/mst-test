import React from 'react';
import './App.css';
import { Provider } from 'mobx-react'
import { setupRootStore } from './mst/setup'
import { Company } from './components/Company';

interface AppProps {

}

const App = (props: AppProps) => {
  const { rootTree } = setupRootStore()

  return (
    <Provider rootTree={rootTree}>
      <Company />
    </Provider>
  );
}

export default App;
