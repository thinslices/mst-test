import { CompanyModel, Company } from '../mst/index'
import { getSnapshot } from 'mobx-state-tree'

let company: Company
beforeEach(() => {
    company = CompanyModel.create({
        id: '1',
        name: 'Penta'
    })
})
describe('Testing Company Model', () => {
    it('Should create from company', () => {
        expect(company).toBeTruthy()
    })

    it('Should add 1 new employee', () => {
        company.addNewEmployee({
            firstName: 'John',
            lastName: 'Doe'
        })

        expect(company.employeesTotal).toEqual(1)
        expect(company.employees.length).toEqual(1)
    })

    it('Expects to respect snapShot format', () => {
        company.addNewEmployee({
            id: '1',
            firstName: 'John',
            lastName: 'Doe'
        })

        expect(getSnapshot(company)).toEqual({
            id: '1',
            name: 'Penta',
            employees: [
                {
                    id: '1',
                    firstName: 'John',
                    lastName: 'Doe',
                    isHealthy: true
                }
            ]
        })
    })

    it('To filter by name', () => {
        company = CompanyModel.create({
            name: 'Penta'
        }, {})
    })
})