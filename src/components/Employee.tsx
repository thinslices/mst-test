import React from 'react'
import { Employee } from '../mst'
import { observer } from 'mobx-react'

interface EmployeeProps {
    employee: Employee
}

export const EmployeeComponent = observer((props: EmployeeProps) => {
    const { firstName, lastName, isHealthy, toggleHealth } = props.employee

    return <div>
        <p>First name: {firstName}</p>
        <p>Last name: {lastName}</p>
        <p>Healthy?: {isHealthy ? 'Of course' : '... :|'}</p>
        <button onClick={toggleHealth}>{isHealthy ? 'Just got sick' : 'Is healthy again! Yey!'}</button>
    </div>
})