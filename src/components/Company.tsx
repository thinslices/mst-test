import React, { useState } from "react"
import { inject, observer } from "mobx-react"
import { Root, EmployeeSnapshot } from "../mst"
import { EmployeeComponent } from "./Employee"

interface CompanyProps {
    rootTree?: Root
}

export const Company = inject('rootTree')(observer((props: CompanyProps) => {
    const { company } = props.rootTree!
    const [employee, setEmployee]: [EmployeeSnapshot, any] = useState({ firstName: '', lastName: '' })
    const [filter, setFilter] = useState('')

    const changeFirstName = (event: any) => {
        setEmployee({
            ...employee,
            firstName: event.target.value
        })
    }

    const changeLastName = (event: any) => {
        setEmployee({
            ...employee,
            lastName: event.target.value
        })
    }

    const saveEmployeeData = (event: any) => {
        event.preventDefault()

        props.rootTree!.company.addNewEmployee(employee)

        setEmployee({
            firstName: '',
            lastName: ''
        })
    }

    const filterByname = (event: any) => {
        setFilter(event.target.value)
    }

    return <div>
        <h1>Hi {company.name}</h1>
        <h3>Total number of employees: {company.employeesTotal}</h3>
        <button onClick={company.getEmployeesFromBE}>Load from BE</button>
        <hr />
        <form onSubmit={saveEmployeeData}>
            <p>Add new employee</p>
            <p>First name:</p>
            <input value={employee.firstName} onChange={changeFirstName} />
            <p>Last name:</p>
            <input value={employee.lastName} onChange={changeLastName} />
            <br />
            <button>Save!</button>
        </form>

        <hr />
        <input placeholder='Search by name' onChange={filterByname} />
        {company.filterByName(filter).map(employee => <EmployeeComponent employee={employee} key={employee.id} />)}
    </div>
}))
