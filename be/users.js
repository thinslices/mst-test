module.exports = {
    USERS: [
        {
            first_name: "Sick",
            last_name: "Doe",
            is_healty: false,
            random_attribute: 'test this',
            random_object1: {
                test: 'this',
                testB: false
            },
            random_array: ['test', 'this'],
            random_atribute3: 'test thas',
        },
        {
            first_name: "Bogdan",
            last_name: "Balan",
            is_healty: true,
            random_attribute: 'test this',
            random_object1: {
                test: 'this',
                testB: false
            },
            random_array: ['test', 'this'],
            random_atribute3: 'test thas',
        },
        {
            first_name: "Daniel",
            last_name: "Sandu",
            is_healty: true,
            random_attribute: 'test this',
            random_object1: {
                test: 'this',
                testB: false
            },
            random_array: ['test', 'this'],
            random_atribute3: 'test thas',
        },
        {
            first_name: "Gabriela",
            last_name: "Macovei",
            is_healty: true,
            random_attribute: 'test this',
            random_object1: {
                test: 'this',
                testB: false
            },
            random_array: ['test', 'this'],
            random_atribute3: 'test thas',
        },
        {
            first_name: "Danut",
            last_name: "Diac",
            is_healty: true,
            random_attribute: 'test this',
            random_object1: {
                test: 'this',
                testB: false
            },
            random_array: ['test', 'this'],
            random_atribute3: 'test thas',
        },
        {
            first_name: "Ioana",
            last_name: "Vasiliu",
            is_healty: true,
            random_attribute: 'test this',
            random_object1: {
                test: 'this',
                testB: false
            },
            random_array: ['test', 'this'],
            random_atribute3: 'test thas',
        },
        {
            first_name: "Mihai",
            last_name: "Bojescu",
            is_healty: true,
            random_attribute: 'test this',
            random_object1: {
                test: 'this',
                testB: false
            },
            random_array: ['test', 'this'],
            random_atribute3: 'test thas',
        },
        {
            first_name: "Mihaela",
            last_name: "Vicol",
            is_healty: true,
            random_attribute: 'test this',
            random_object1: {
                test: 'this',
                testB: false
            },
            random_array: ['test', 'this'],
            random_atribute3: 'test thas',
        },
        {
            first_name: "Lucian",
            last_name: "Vasiliu",
            is_healty: true,
            random_attribute: 'test this',
            random_object1: {
                test: 'this',
                testB: false
            },
            random_array: ['test', 'this'],
            random_atribute3: 'test thas',
        },
    ]
}