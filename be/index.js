const Koa = require('koa')
const cors = require('@koa/cors');
const { USERS } = require('./users')

const app = new Koa()

app.use(cors())
app.use(async ctx => {
    switch (ctx.path) {
        case '/users':
            ctx.body = USERS
            break;
        default:
            ctx.body = 'Hello World';
    }
});

app.listen(3001);

